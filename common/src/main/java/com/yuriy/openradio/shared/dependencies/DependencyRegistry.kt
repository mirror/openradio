/*
 * Copyright 2021 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.openradio.shared.dependencies

import android.app.UiModeManager
import android.content.Context
import android.content.res.Configuration
import android.net.ConnectivityManager
import androidx.multidex.MultiDexApplication
import com.google.android.gms.cast.framework.CastContext
import com.google.android.gms.common.GoogleApiAvailability
import com.yuriy.openradio.shared.model.api.ApiServiceProvider
import com.yuriy.openradio.shared.model.api.ApiServiceProviderImpl
import com.yuriy.openradio.shared.model.media.OpenRadioEqualizer
import com.yuriy.openradio.shared.model.media.OpenRadioEqualizerImpl
import com.yuriy.openradio.shared.model.media.RadioStationManager
import com.yuriy.openradio.shared.model.net.Downloader
import com.yuriy.openradio.shared.model.net.HTTPDownloaderImpl
import com.yuriy.openradio.shared.model.net.NetworkMonitor
import com.yuriy.openradio.shared.model.parser.DataParser
import com.yuriy.openradio.shared.model.parser.JsonDataParserImpl
import com.yuriy.openradio.shared.model.storage.FavoritesStorage
import com.yuriy.openradio.shared.model.storage.LatestRadioStationStorage
import com.yuriy.openradio.shared.model.storage.LocalRadioStationsStorage
import com.yuriy.openradio.shared.model.storage.images.ImagesDatabase
import com.yuriy.openradio.shared.utils.AppLogger
import java.util.concurrent.atomic.AtomicBoolean

object DependencyRegistry {

    private lateinit var sNetMonitor: NetworkMonitor
    private lateinit var sDownloader: Downloader
    private lateinit var sParser: DataParser
    private lateinit var sProvider: ApiServiceProvider
    private lateinit var sFavoritesStorage: FavoritesStorage
    private lateinit var sLocalRadioStationsStorage: LocalRadioStationsStorage
    private lateinit var sLatestRadioStationStorage: LatestRadioStationStorage
    private lateinit var sImagesDatabase: ImagesDatabase
    private lateinit var sEqualizer: OpenRadioEqualizer
    private lateinit var sRadioStationManager: RadioStationManager

    /**
     * Flag that indicates whether application runs over normal Android or Android TV.
     */
    private var sIsTv = AtomicBoolean(false)
    private var sIsCar = AtomicBoolean(false)
    private var sIsCastAvailable = AtomicBoolean(false)

    @Volatile
    private var sInit = AtomicBoolean(false)

    fun init(context: Context) {
        if (sInit.get()) {
            return
        }

        val orientationStr: String
        val orientation = context.resources.configuration.orientation
        orientationStr = if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            "Landscape"
        } else {
            "Portrait"
        }
        val uiModeManager = context.getSystemService(MultiDexApplication.UI_MODE_SERVICE) as UiModeManager
        AppLogger.d("CurrentModeType:${uiModeManager.currentModeType}")
        isTv = if (uiModeManager.currentModeType == Configuration.UI_MODE_TYPE_TELEVISION) {
            AppLogger.d("Running on TV Device in $orientationStr")
            true
        } else {
            AppLogger.d("Running on non-TV Device")
            false
        }
        isCar = if (uiModeManager.currentModeType == Configuration.UI_MODE_TYPE_CAR) {
            AppLogger.d("Running on Car Device in $orientationStr")
            true
        } else {
            AppLogger.d("Running on non-Car Device")
            false
        }

        val isGoogleApiAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)
        isCastAvailable = try {
            CastContext.getSharedInstance(context)
            true
        } catch (e: Exception) {
            AppLogger.e("Cast API availability exception '${e.message}'")
            false
        }
        AppLogger.i("Google API:$isGoogleApiAvailable")
        AppLogger.i("Cast API:$isCastAvailable")

        sImagesDatabase = ImagesDatabase.getInstance(context)
        sNetMonitor = NetworkMonitor(context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        sNetMonitor.init()
        sDownloader = HTTPDownloaderImpl()
        sParser = JsonDataParserImpl()
        sProvider = ApiServiceProviderImpl(context, sParser, sNetMonitor)
        sFavoritesStorage = FavoritesStorage()
        sLatestRadioStationStorage = LatestRadioStationStorage()
        sLocalRadioStationsStorage = LocalRadioStationsStorage(
            sFavoritesStorage, sLatestRadioStationStorage
        )
        sEqualizer = OpenRadioEqualizerImpl.makeInstance(context)
        sRadioStationManager = RadioStationManager(
            sProvider, sDownloader, sLocalRadioStationsStorage, sFavoritesStorage, sImagesDatabase
        )

        sInit.set(true)
    }

    var isTv: Boolean
        get() = sIsTv.get()
        set(value) {
            sIsTv.set(value)
        }

    var isCar: Boolean
        get() = sIsCar.get()
        set(value) {
            sIsCar.set(value)
        }

    var isCastAvailable: Boolean
        get() = sIsCastAvailable.get()
        private set(value) {
            sIsCastAvailable.set(value)
        }

    fun injectNetworkMonitor(dependency: NetworkMonitorDependency) {
        dependency.configureWith(sNetMonitor)
    }

    fun injectDownloader(dependency: DownloaderDependency) {
        dependency.configureWith(sDownloader)
    }

    fun injectParser(dependency: ParserDependency) {
        dependency.configureWith(sParser)
    }

    fun injectProvider(dependency: ApiServiceProviderDependency) {
        dependency.configureWith(sProvider)
    }

    fun injectFavoritesStorage(dependency: FavoritesStorageDependency) {
        dependency.configureWith(sFavoritesStorage)
    }

    fun injectLocalRadioStationsStorage(dependency: LocalRadioStationsStorageDependency) {
        dependency.configureWith(sLocalRadioStationsStorage)
    }

    fun injectLatestRadioStationStorage(dependency: LatestRadioStationStorageDependency) {
        dependency.configureWith(sLatestRadioStationStorage)
    }

    fun injectImagesDatabase(dependency: ImagesDatabaseDependency) {
        dependency.configureWith(sImagesDatabase)
    }

    fun injectEqualizer(dependency: EqualizerDependency) {
        dependency.configureWith(sEqualizer)
    }

    fun injectManageRadioStationPresenter(dependency: ManageRadioStationDependency) {
        dependency.configureWith(sRadioStationManager)
    }
}
