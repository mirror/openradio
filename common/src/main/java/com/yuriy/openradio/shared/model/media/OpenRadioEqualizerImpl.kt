/*
 * Copyright 2020-2022 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.openradio.shared.model.media

import android.content.Context
import android.media.audiofx.Equalizer
import com.yuriy.openradio.shared.model.storage.EqualizerStorage
import com.yuriy.openradio.shared.model.translation.EqualizerJsonStateSerializer
import com.yuriy.openradio.shared.model.translation.EqualizerStateJsonDeserializer
import com.yuriy.openradio.shared.utils.AppLogger
import com.yuriy.openradio.shared.utils.AppUtils
import com.yuriy.openradio.shared.vo.EqualizerState

class OpenRadioEqualizerImpl private constructor(private val mContext: Context): OpenRadioEqualizer {

    private var mEqualizer: Equalizer? = null

    override fun init(audioSessionId: Int) {
        if (mEqualizer != null) {
            AppLogger.e("$CLASS_NAME can not init with null equalizer")
            return
        }
        try {
            mEqualizer = Equalizer(10, audioSessionId)
        } catch (e: Exception) {
            AppLogger.e("$CLASS_NAME exception while init", e)
            mEqualizer = null
            EqualizerStorage.saveEqualizerState(mContext, AppUtils.EMPTY_STRING)
            return
        }
        //TODO: Do state operations in separate thread.
        if (EqualizerStorage.isEmpty(mContext)) {
            saveState()
        } else {
            loadState {}
        }
    }

    override fun deinit() {
        if (mEqualizer == null) {
            AppLogger.e("$CLASS_NAME can not deinit with null equalizer")
            return
        }
        mEqualizer?.release()
        mEqualizer = null
    }

    override fun saveState() {
        if (mEqualizer == null) {
            AppLogger.e("$CLASS_NAME can not save state for null equalizer")
            return
        }
        var state: EqualizerState? = null
        try {
            state = EqualizerState.createState(mEqualizer!!)
        } catch (e: IllegalArgumentException) {
            AppLogger.e("Can not create state from $mEqualizer", e)
        } catch (e: IllegalStateException) {
            AppLogger.e("Can not create state from $mEqualizer", e)
        } catch (e: UnsupportedOperationException) {
            AppLogger.e("Can not create state from $mEqualizer", e)
        } catch (e: RuntimeException) {
            // Some times this happen with "AudioEffect: set/get parameter error"
            AppLogger.e("$CLASS_NAME can not create state from $mEqualizer", e)
        }
        if (state != null) {
            saveState(mContext, state)
        }
    }

    override fun loadState(onSuccess: () -> Unit) {
        if (mEqualizer == null) {
            AppLogger.e("$CLASS_NAME can not load state for null equalizer")
            return
        }
        val state = loadState(mContext)
        try {
            mEqualizer?.enabled = false
            mEqualizer?.enabled = true
            mEqualizer?.usePreset(state.currentPreset)
            saveState(mContext, EqualizerState(mEqualizer!!))
            onSuccess()
        } catch (e: Exception) {
            AppLogger.e("$CLASS_NAME apply state", e)
        }
        state.printState()
    }

    companion object {

        private val CLASS_NAME = OpenRadioEqualizerImpl::class.java.simpleName

        fun makeInstance(context: Context): OpenRadioEqualizer {
            return OpenRadioEqualizerImpl(context)
        }

        fun loadState(context: Context): EqualizerState {
            val deserializer = EqualizerStateJsonDeserializer()
            return deserializer.deserialize(context, EqualizerStorage.loadEqualizerState(context))
        }

        fun saveState(context: Context, state: EqualizerState) {
            val serializer = EqualizerJsonStateSerializer()
            EqualizerStorage.saveEqualizerState(context, serializer.serialize(state))
        }
    }
}
