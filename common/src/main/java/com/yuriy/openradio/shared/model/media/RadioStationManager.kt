/*
 * Copyright 2022 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.openradio.shared.model.media

import android.content.Context
import com.yuriy.openradio.shared.model.api.ApiServiceProvider
import com.yuriy.openradio.shared.model.net.Downloader
import com.yuriy.openradio.shared.model.storage.FavoritesStorage
import com.yuriy.openradio.shared.model.storage.LocalRadioStationsStorage
import com.yuriy.openradio.shared.model.storage.images.ImagesDatabase
import com.yuriy.openradio.shared.model.storage.images.ImagesStore
import com.yuriy.openradio.shared.service.OpenRadioService
import com.yuriy.openradio.shared.utils.AppUtils
import com.yuriy.openradio.shared.utils.MediaIdHelper
import com.yuriy.openradio.shared.utils.RadioStationValidator
import com.yuriy.openradio.shared.vo.MediaStream
import com.yuriy.openradio.shared.vo.RadioStation
import com.yuriy.openradio.shared.vo.RadioStationToAdd
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RadioStationManager(
    provider: ApiServiceProvider,
    downloader: Downloader,
    private val mLocalRadioStationsStorage: LocalRadioStationsStorage,
    private val mFavoritesStorage: FavoritesStorage,
    private val mImagesDatabase: ImagesDatabase
) {

    private var mUiScope = CoroutineScope(Dispatchers.Main)
    private var mScope = CoroutineScope(Dispatchers.IO)
    private var mRadioStationValidator = RadioStationValidator(provider, downloader, mUiScope, mScope)

    fun addRadioStation(
        context: Context, rsToAdd: RadioStationToAdd,
        onSuccess: (msg: String) -> Unit,
        onFailure: (msg: String) -> Unit
    ) {
        mRadioStationValidator.validate(
            context, rsToAdd,
            { msg ->
                onSuccess(msg)
                run {
                    val radioStation = RadioStation.makeDefaultInstance(
                        mLocalRadioStationsStorage.getId(context)
                    )
                    radioStation.name = rsToAdd.name
                    radioStation.mediaStream.setVariant(MediaStream.BITRATE_DEFAULT, rsToAdd.url)
                    radioStation.imageUrl = rsToAdd.imageLocalUrl
                    radioStation.genre = rsToAdd.genre
                    radioStation.country = rsToAdd.country
                    radioStation.isLocal = true
                    mLocalRadioStationsStorage.add(radioStation, context)
                    if (rsToAdd.isAddToFav) {
                        mFavoritesStorage.add(radioStation, context)
                    }
                    context.startService(
                        OpenRadioService.makeNotifyChildrenChangedIntent(
                            context,
                            MediaIdHelper.MEDIA_ID_ROOT
                        )
                    )
                    onSuccess("Radio Station added successfully")
                }
            },
            { msg -> onFailure(msg) },
            { msg -> onFailure(msg) }
        )
    }

    fun editRadioStation(
        context: Context, mediaId: String?, rsToAdd: RadioStationToAdd,
        onSuccess: (msg: String) -> Unit,
        onFailure: (msg: String) -> Unit
    ) {
        mScope.launch {
            mImagesDatabase.rsImageDao().delete(mediaId.toString())

            val result = mLocalRadioStationsStorage.update(
                mediaId, context, rsToAdd.name, rsToAdd.url, rsToAdd.imageLocalUrl, rsToAdd.genre, rsToAdd.country,
                rsToAdd.isAddToFav
            )
            if (result) {
                context.startService(
                    OpenRadioService.makeNotifyChildrenChangedIntent(
                        context,
                        MediaIdHelper.MEDIA_ID_LOCAL_RADIO_STATIONS_LIST
                    )
                )
                mUiScope.launch { onSuccess("Radio Station updated successfully") }
            } else {
                mUiScope.launch { onFailure("Can not update Radio Station") }
            }
        }
    }

    fun removeRadioStation(context: Context?, mediaId: String?) {
        if (context == null) {
            return
        }
        if (mediaId.isNullOrEmpty()) {
            return
        }
        mScope.launch {
            val radioStation = mLocalRadioStationsStorage[mediaId, context]
            if (radioStation != RadioStation.INVALID_INSTANCE) {
                context.contentResolver.delete(ImagesStore.getDeleteUri(mediaId), AppUtils.EMPTY_STRING, emptyArray())
                mLocalRadioStationsStorage.remove(radioStation, context)
            }
            context.startService(
                OpenRadioService.makeRemoveByMediaIdIntent(
                    context, mediaId
                )
            )
            context.startService(
                OpenRadioService.makeNotifyChildrenChangedIntent(
                    context, MediaIdHelper.MEDIA_ID_LOCAL_RADIO_STATIONS_LIST
                )
            )
        }
    }
}
