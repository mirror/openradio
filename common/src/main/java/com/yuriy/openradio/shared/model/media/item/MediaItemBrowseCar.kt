/*
 * Copyright 2015-2022 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.openradio.shared.model.media.item

import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaDescriptionCompat
import com.yuriy.openradio.R
import com.yuriy.openradio.shared.model.media.item.MediaItemCommand.IUpdatePlaybackState
import com.yuriy.openradio.shared.model.storage.AppPreferencesManager
import com.yuriy.openradio.shared.service.LocationService
import com.yuriy.openradio.shared.utils.MediaIdHelper
import com.yuriy.openradio.shared.utils.MediaItemHelper
import com.yuriy.openradio.shared.vo.RadioStation
import java.util.*

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 8/31/15
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 * [MediaItemBrowseCar] is concrete implementation of the [MediaItemCommand] that
 * designed to prepare data to display Browse menu items for Car display.
 */
class MediaItemBrowseCar : MediaItemCommand {

    override fun execute(playbackStateListener: IUpdatePlaybackState, dependencies: MediaItemCommandDependencies) {
        val context = dependencies.context
        dependencies.radioStationsStorage.clear()
        dependencies.result.detach()

        if (AppPreferencesManager.lastKnownRadioStationEnabled(context)) {
            val latestRadioStation = dependencies.mLatestRadioStationStorage[dependencies.context]
            if (latestRadioStation != RadioStation.INVALID_INSTANCE) {
                dependencies.radioStationsStorage.add(latestRadioStation)
            }
        }

        // Popular Radio Stations
        val popBuilder = MediaDescriptionCompat.Builder()
            .setMediaId(MediaIdHelper.MEDIA_ID_POPULAR_STATIONS)
            .setTitle(context.getString(R.string.popular_stations_title))
        val bundlePop = Bundle()
        MediaItemHelper.setDrawableId(bundlePop, R.drawable.ic_trending_up_black_24dp)
        popBuilder.setExtras(bundlePop)
        dependencies.addMediaItem(
            MediaBrowserCompat.MediaItem(popBuilder.build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE)
        )

        // Worldwide Stations
        val catBuilder = MediaDescriptionCompat.Builder()
            .setMediaId(MediaIdHelper.MEDIA_ID_ALL_CATEGORIES)
            .setTitle(context.getString(R.string.all_categories_title))
        val bundle = Bundle()
        MediaItemHelper.setDrawableId(bundle, R.drawable.ic_all_categories)
        catBuilder.setExtras(bundle)
        dependencies.addMediaItem(
            MediaBrowserCompat.MediaItem(catBuilder.build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE)
        )

        // All countries list
        val countriesBuilder = MediaDescriptionCompat.Builder()
            .setMediaId(MediaIdHelper.MEDIA_ID_COUNTRIES_LIST)
            .setTitle(context.getString(R.string.countries_list_title))
        val bundleCounties = Bundle()
        MediaItemHelper.setDrawableId(bundleCounties, R.drawable.ic_public_black_24dp)
        countriesBuilder.setExtras(bundleCounties)
        dependencies.addMediaItem(
            MediaBrowserCompat.MediaItem(countriesBuilder.build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE)
        )

        // If the Country code is known:
        val locationStr = context.getString(R.string.default_country_use_location)
        if (dependencies.countryCode.isNotEmpty() && dependencies.countryCode != locationStr) {
            val identifier = context.resources.getIdentifier(
                "flag_" + dependencies.countryCode.lowercase(Locale.ROOT),
                "drawable", context.packageName
            )
            val countryBundle = Bundle()
            MediaItemHelper.setDrawableId(countryBundle, identifier)
            dependencies.addMediaItem(
                MediaBrowserCompat.MediaItem(
                    MediaDescriptionCompat.Builder()
                        .setMediaId(MediaIdHelper.MEDIA_ID_COUNTRY_STATIONS)
                        .setTitle(LocationService.COUNTRY_CODE_TO_NAME[dependencies.countryCode])
                        .setExtras(countryBundle)
                        .build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
                )
            )
        }

        // Show Local Radio Stations if they are exists
        if (!dependencies.mLocalRadioStationsStorage.isLocalsEmpty(context)) {
            // Locals list
            val localsBuilder = MediaDescriptionCompat.Builder()
                .setMediaId(MediaIdHelper.MEDIA_ID_LOCAL_RADIO_STATIONS_LIST)
                .setTitle(context.getString(R.string.local_radio_stations_list_title))
            val localsBundle = Bundle()
            MediaItemHelper.setDrawableId(localsBundle, R.drawable.ic_locals)
            localsBuilder.setExtras(localsBundle)
            dependencies.addMediaItem(
                MediaBrowserCompat.MediaItem(localsBuilder.build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE)
            )
        }
        dependencies.result.sendResult(dependencies.mediaItems)
        dependencies.resultListener.onResult()
    }
}
