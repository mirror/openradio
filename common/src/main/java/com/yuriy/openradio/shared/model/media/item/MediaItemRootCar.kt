/*
 * Copyright 2015-2022 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.openradio.shared.model.media.item

import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaDescriptionCompat
import com.yuriy.openradio.R
import com.yuriy.openradio.shared.model.media.item.MediaItemCommand.IUpdatePlaybackState
import com.yuriy.openradio.shared.model.storage.AppPreferencesManager
import com.yuriy.openradio.shared.utils.MediaIdHelper
import com.yuriy.openradio.shared.utils.MediaItemHelper
import com.yuriy.openradio.shared.vo.RadioStation

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 8/31/15
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 * [MediaItemRootCar] is concrete implementation of the [MediaItemCommand] that
 * designed to prepare data to display root menu items for Car display.
 */
class MediaItemRootCar : MediaItemCommand {

    override fun execute(playbackStateListener: IUpdatePlaybackState, dependencies: MediaItemCommandDependencies) {
        val context = dependencies.context
        dependencies.radioStationsStorage.clear()
        dependencies.result.detach()

        if (AppPreferencesManager.lastKnownRadioStationEnabled(context)) {
            val latestRadioStation = dependencies.mLatestRadioStationStorage[dependencies.context]
            if (latestRadioStation != RadioStation.INVALID_INSTANCE) {
                dependencies.radioStationsStorage.add(latestRadioStation)
            }
        }

        // Show Favorites if they are exists.
        if (!dependencies.favoritesStorage.isFavoritesEmpty(context)) {
            // Favorites list.
            val favBuilder = MediaDescriptionCompat.Builder()
                .setMediaId(MediaIdHelper.MEDIA_ID_FAVORITES_LIST)
                .setTitle(context.getString(R.string.favorites_list_title))
            val bundle = Bundle()
            MediaItemHelper.setDrawableId(bundle, R.drawable.ic_stars_black_24dp)
            favBuilder.setExtras(bundle)
            dependencies.addMediaItem(
                MediaBrowserCompat.MediaItem(favBuilder.build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE)
            )
        }

        // Recently added Radio Stations.
        val recentBuilder = MediaDescriptionCompat.Builder()
            .setMediaId(MediaIdHelper.MEDIA_ID_RECENT_ADDED_STATIONS)
            .setTitle(context.getString(R.string.new_stations_title))
        val bundleRecent = Bundle()
        MediaItemHelper.setDrawableId(bundleRecent, R.drawable.ic_fiber_new_black_24dp)
        recentBuilder.setExtras(bundleRecent)
        dependencies.addMediaItem(
            MediaBrowserCompat.MediaItem(recentBuilder.build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE)
        )

        // Browse category to provide the rest of categories.
        dependencies.addMediaItem(
            MediaBrowserCompat.MediaItem(
                MediaDescriptionCompat.Builder()
                    .setMediaId(MediaIdHelper.MEDIA_ID_BROWSE)
                    .setTitle(context.getString(R.string.browse_title))
                    .build(),
                MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
            )
        )

        dependencies.result.sendResult(dependencies.mediaItems)
        dependencies.resultListener.onResult()
    }
}
