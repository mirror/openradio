/*
 * Copyright 2022 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.openradio.shared.utils

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import androidx.core.content.ContextCompat
import com.yuriy.openradio.shared.notification.MediaNotification
import com.yuriy.openradio.shared.service.OpenRadioService

object ServiceUtils {

    fun startForegroundSafe(context: Context, intent: Intent) {
        try {
            ContextCompat.startForegroundService(context, intent)
        } catch (exception: RuntimeException) {
            AppLogger.e("Start foreground service failed", exception)
        }
    }

    /**
     * Start the service if it should be running but it's not currently.
     * Starting the service in the foreground causes notification flicker if there is no scheduled action.
     * Starting it in the background throws an exception if the app is in the background too
     * (e.g. if device screen is locked).
     */
    fun startForegroundSafe(context: Context, service: Service, notification: Notification) {
        try {
            if (AppUtils.hasVersionQ()) {
                service.startForeground(
                    MediaNotification.NOTIFICATION_ID, notification,
                    ServiceInfo.FOREGROUND_SERVICE_TYPE_MANIFEST
                )
            } else {
                service.startForeground(MediaNotification.NOTIFICATION_ID, notification)
            }
        } catch (exception: RuntimeException) {
            startForegroundSafe(context, OpenRadioService.makePlayLastPlayedItemIntent(context))
        }
    }
}
